import { createStackNavigator } from '@react-navigation/stack';
import WorkoutScreen from '../screens/WorkoutScreen'
import WorkoutAddScreen from '../screens/WorkoutAddScreen'
import WorkoutEditScreen from '../screens/WorkoutEditScreen'
import WorkoutRepsScreen from '../screens/WorkoutRepsScreen'
import WorkoutRestScreen from '../screens/WorkoutRestScreen'
import WorkoutResultsScreen from '../screens/WorkoutResultsScreen'

const Stack = createStackNavigator();

export default function WorkoutNavigator() {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Workout" component={WorkoutScreen} />
            <Stack.Screen name="WorkoutAdd" component={WorkoutAddScreen} />
            <Stack.Screen name="WorkoutEditScreen" component={WorkoutEditScreen} />
            <Stack.Screen name="WorkoutRepsScreen" component={WorkoutRepsScreen} />
            <Stack.Screen name="WorkoutRestScreen" component={WorkoutRestScreen} />
            <Stack.Screen name="WorkoutResultsScreen" component={WorkoutResultsScreen} />
        </Stack.Navigator>
    );
}