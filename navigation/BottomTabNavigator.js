import * as React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Pressable } from "react-native";
import { createStackNavigator } from '@react-navigation/stack';
import WorkoutsScreen from '../screens/WorkoutsScreen'
import ProgressesScreen from '../screens/ProgressesScreen'
import Icon from 'react-native-vector-icons/FontAwesome5';
import { getFocusedRouteNameFromRoute } from '@react-navigation/native';

const BottomTab = createBottomTabNavigator();

export default function BottomTabNavigator({ navigation, route }) {

    // This is the important part to be added 
    React.useLayoutEffect(() => {
        const routeName = getFocusedRouteNameFromRoute(route) ?? 'Workouts';
        navigation.setOptions({
            headerRight: (props) =>
                routeName === 'Workouts' ?
                    <Pressable onPress={() => alert('This is a button!')}>
                        <Icon name="plus" size={30} color="#900" />
                    </Pressable>
                    : null
        });
    }, [navigation, route]);

    return <BottomTab.Navigator
        initialRouteName="Workouts">
        <BottomTab.Screen
            name="Workouts"
            component={WorkoutsScreen}
            options={{
                tabBarIcon: () => <Icon name="comments" size={30} color="#900" />
            }}
        />
        <BottomTab.Screen
            name="Progresses"
            component={ProgressesScreen}
            options={{
                tabBarIcon: () => <Icon name="list" size={30} color="#900" />,
            }}
        />
    </BottomTab.Navigator>
}