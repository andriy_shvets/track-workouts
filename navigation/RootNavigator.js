import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer, getFocusedRouteNameFromRoute } from '@react-navigation/native';
import BottomTabNavigator from './BottomTabNavigator'
import WorkoutNavigator from './WorkoutNavigator'
import WorkoutProgressScreen from '../screens/WorkoutProgressScreen'
import { View } from 'react-native';

const Stack = createStackNavigator();

function getHeaderTitle(route) {
    // If the focused route is not found, we need to assume it's the initial screen
    // This can happen during if there hasn't been any navigation inside the screen
    // In our case, it's "Feed" as that's the first screen inside the navigator
    const routeName = getFocusedRouteNameFromRoute(route) ?? 'Workouts';

    switch (routeName) {
        case 'Workouts':
            return 'Workouts';
        case 'Progresses':
            return 'Progresses';
        case 'Workout':
            return 'Workout';
        case 'WorkoutAdd':
            return 'WorkoutAdd';
        case 'WorkoutEditScreen':
            return 'WorkoutEditScreen';
        case 'WorkoutRepsScreen':
            return 'WorkoutRepsScreen';
        case 'WorkoutRestScreen':
            return 'WorkoutRestScreen';
        case 'WorkoutResultsScreen':
            return 'WorkoutResultsScreen';
    }
}

export default function RootNavigator() {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name="TabNavigator" component={BottomTabNavigator} options={({ route }) => ({
                    headerTitle: getHeaderTitle(route),
                    headerTitleAlign: 'center'
                })} />
                <Stack.Screen name="Workout" component={WorkoutNavigator} options={{ headerTitleStyle: { alignSelf: 'center' } }} />
                <Stack.Screen name="WorkoutProgress" component={WorkoutProgressScreen} options={{ headerTitleStyle: { alignSelf: 'center' } }} />
            </Stack.Navigator>
        </NavigationContainer>
    );
}